﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class DirectionalAnimation : DirectionalComponent {

	public float frameInterval = 0.5f;

	public Sprite[] leftIdleFrames;
	public Sprite[] rightIdleFrames;
	public Sprite[] upIdleFrames;
	public Sprite[] downIdleFrames;

	public Sprite[] leftWalkFrames;
	public Sprite[] rightWalkFrames;
	public Sprite[] upWalkFrames;
	public Sprite[] downWalkFrames;

	private float _speed = 0;
	public float speed {
		get { return _speed; }
		set {
			if (_speed > 0.1f && value < 0.1f) {
				_speed = value;
				UpdateFrame();
			}
			else _speed = value;
		}
	}

	private int currentFrame = 0;
	private Sprite[] _currentIdleFrames;
	private Sprite[] currentIdleFrames {
		get { return _currentIdleFrames; }
		set {
			if (_currentIdleFrames != value) {
				_currentIdleFrames = value;
				UpdateFrame();
			}
		}
	}

	private Sprite[] _currentWalkFrames;
	private Sprite[] currentWalkFrames {
		get { return _currentWalkFrames; }
		set {
			if (_currentWalkFrames != value) {
				_currentWalkFrames = value;
				UpdateFrame();				
			}
		}
	}

	private SpriteRenderer sprite;

	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
		currentIdleFrames = upIdleFrames;
		currentWalkFrames = upWalkFrames;
	}

	void OnEnable () {
		StartCoroutine("AnimationCoroutine");
	}

	protected override void SetLeft() {
		currentIdleFrames = leftIdleFrames;
		currentWalkFrames = leftWalkFrames;
	}

	protected override void SetRight() {
		currentIdleFrames = rightIdleFrames;
		currentWalkFrames = rightWalkFrames;
	}

	protected override void SetUp() {
		currentIdleFrames = upIdleFrames;
		currentWalkFrames = upWalkFrames;
	}

	protected override void SetDown() {
		currentIdleFrames = downIdleFrames;
		currentWalkFrames = downWalkFrames;
	}

	IEnumerator AnimationCoroutine () {
		while (enabled) {
			UpdateFrame();
			yield return new WaitForSeconds(frameInterval);
		}
	}

	void UpdateFrame () {
		if (speed > 0.1f) {
			currentFrame %= currentWalkFrames.Length;
			sprite.sprite = currentWalkFrames[currentFrame];
		}
		else {
			currentFrame %= currentIdleFrames.Length;
			sprite.sprite = currentIdleFrames[currentFrame];
		}
		currentFrame++;
	}
}
