﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Paraphernalia.Extensions;
using Paraphernalia.Components;
using Paraphernalia.Utils;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(DirectionalAnimation))]
public class PlayerController : MonoBehaviour {
	
	public float speed = 1;
	private Vector2 heading;
	private Vector2 velocity;

	private Rigidbody2D body;
	private DirectionalAnimation anim;

	void Awake () {
		body = GetComponent<Rigidbody2D>();
		anim = GetComponent<DirectionalAnimation>();
	}

	void Update () {
		GetInput();
		UpdateDirection();
		Move();
	}

	void GetInput() {
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
		
		if (Mathf.Abs(x) > Mathf.Abs(y)) {
			y = 0;
			heading = Vector2.right * Mathf.Sign(x);
		}
		else if (Mathf.Abs(x) < Mathf.Abs(y)) {
			x = 0;
			heading = Vector2.up * Mathf.Sign(y);
		}
		else {
			x = 0;
		}

		velocity = new Vector2(x, y) * speed;
	}

	void UpdateDirection() {
		anim.speed = velocity.magnitude;
		anim.SetDirection(heading);
	}

	void Move () {
		body.velocity = velocity;
	}
}
