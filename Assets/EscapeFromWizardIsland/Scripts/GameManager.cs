﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Paraphernalia.Components;
using Paraphernalia.Extensions;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance {
		get { return _instance;	}
	}

	public AudioClip overworldMusic;
	public AudioClip battleMusic;

	public PlayerSheet[] party;
	public EnemySheet[] enemies;

	private string currentLevel = "Test";
	private GameObject _currentLevelRoot;
	private GameObject currentLevelRoot {
		get {
			if (_currentLevelRoot == null || _currentLevelRoot.name != currentLevel) {
				_currentLevelRoot = GameObject.Find(currentLevel);
			}
			return _currentLevelRoot;
		}
	}

	private GameObject _battleRoot;
	public GameObject battleRoot {
		get {
			if (_battleRoot == null) {
				_battleRoot = GameObject.Find("Battle");
			}
			return _battleRoot;
		}
	}

	void Awake () {
		if (instance == null) {
			_instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (instance != this) {
			Destroy(gameObject);
		}

		_instance.gameObject.GetOrAddComponent<AudioListener>();
	}

	[ContextMenu("HealParty")]
	public void HealParty () {
		foreach (PlayerSheet c in party) {
			c.currentHealth = c.health;
		}
	}

	public static void LoadLevel (string level) {
		instance.currentLevel = level;
		AudioManager.PlayMusic(instance.overworldMusic);
		SceneManager.LoadScene(level);
	}

	public static void StartBattle (EnemySheet[] enemies) {
		AudioManager.PlayMusic(instance.battleMusic);
		instance.enemies = new EnemySheet[enemies.Length];
		for (int i = 0; i < enemies.Length; i++) {
			EnemySheet e = Instantiate(enemies[i]) as EnemySheet;
			e.currentHealth = e.health;
			instance.enemies[i] = e;
		}
		instance.StartCoroutine("StartBattleCoroutine");		
	}

	IEnumerator StartBattleCoroutine () {
		if (instance.battleRoot == null) {
			yield return SceneManager.LoadSceneAsync("Battle", LoadSceneMode.Additive);
		}
		else {
			instance.battleRoot.SetActive(true);
		}
		instance.currentLevelRoot.SetActive(false);
		yield return new WaitForEndOfFrame();
	}

	public static void ExitBattle () {
		AudioManager.PlayMusic(instance.overworldMusic);
		instance.battleRoot.SetActive(false);
		instance.currentLevelRoot.SetActive(true);
	}
}
