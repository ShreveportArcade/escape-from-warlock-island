﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class BattleCharacter : MonoBehaviour {

	public delegate void OnBattleCharacterSelected(CharacterSheet sheet);
	public static OnBattleCharacterSelected onBattleCharacterSelected = delegate {};

	public CharacterSheet sheet;

	private SpriteRenderer sprite;

	private Vector3 defaultPosition;
	private Vector3 defaultScale;

	private bool alreadyDead = false;

	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
	}

	void SetupDefaults () {
		defaultPosition = transform.position;
		defaultScale = transform.localScale;
	}

	public static void SetupAllDefaults () {
		BattleCharacter[] chars = FindObjectsOfType(typeof(BattleCharacter)) as BattleCharacter[];
		foreach (BattleCharacter b in chars) {
			b.SetupDefaults();
		}
	}

	public static void RaycastCharacters () {
		Vector2 pos = (Vector2)BattleUIController.instance.cam.ScreenToWorldPoint(Input.mousePosition);
 		Collider2D[] colliders = Physics2D.OverlapPointAll(pos);
 		foreach (Collider2D c in colliders) {
 			BattleCharacter b = c.gameObject.GetComponent<BattleCharacter>();
 			if (b != null) {
 				onBattleCharacterSelected(b.sheet);
 			}
 		}
	}

	public static void SelectCharacter (CharacterSheet sheet) {
		BattleCharacter[] chars = FindObjectsOfType(typeof(BattleCharacter)) as BattleCharacter[];
		foreach (BattleCharacter b in chars) {
			if (b.sheet == sheet) b.SetToSelectedState();
			else b.SetToDefaultState();
		}
	}

	public static void SetAllToDefault () {
		BattleCharacter[] chars = FindObjectsOfType(typeof(BattleCharacter)) as BattleCharacter[];
		foreach (BattleCharacter b in chars) {
			b.SetToDefaultState();
		}
	}

	public IEnumerator DeathCoroutine () {
		if (!alreadyDead) {
			alreadyDead = true;
			SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
			Color[] colors = System.Array.ConvertAll(sprites, s => s.color);
			for (float t = 0; t < 2; t += Time.deltaTime) {
				float frac = t / 2f;
				sprite.color = Color.Lerp(Color.white, Color.clear, frac);
				for (int i = 0; i < sprites.Length; i++) {
					sprites[i].color = Color.Lerp(colors[i], Color.clear, frac);
				}
				yield return new WaitForEndOfFrame();
			}
		}
	}

	public void SetToDefaultState () {
		transform.position = defaultPosition;
		transform.localScale = defaultScale;
	}

	public void SetToSelectedState () {
		Vector3 v = BattleUIController.instance.cam.WorldToViewportPoint(defaultPosition);
		if (v.y > 0.3f) transform.position = defaultPosition + Vector3.down * 0.1f;
		else transform.position = defaultPosition + Vector3.up * 0.1f;
		transform.localScale = defaultScale + Vector3.one * 0.2f;
	}
}
