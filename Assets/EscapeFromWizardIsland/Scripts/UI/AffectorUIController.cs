﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AffectorUIController : MonoBehaviour {

	public Affector affector;

	private CharacterSheet source;
	private CharacterSheet[] targets;

	public Text promptText;
	public Button confirmButton;

	void OnEnable () {
		promptText.text = affector.name + " - " + affector.description;

		source = BattleUIController.instance.currentCharacter;
		List<CharacterSheet> targetList = new List<CharacterSheet>();
		switch (affector.type) {
			case Affector.Type.AllFriendly:
				targetList.AddRange(BattleUIController.enemies);
				targetList.AddRange(GameManager.instance.party);
				break;

			case Affector.Type.AllEnemy:
				targetList.AddRange(BattleUIController.enemies);
				break;

			case Affector.Type.All:
				targetList.AddRange(GameManager.instance.party);
				break;

			default:
				confirmButton.interactable = false;
				StartCoroutine("CheckForSelectionCoroutine");
				break;
		}

		targets = targetList.ToArray();
		BattleCharacter.onBattleCharacterSelected += OnCharacterSelected;
	}

	void OnDisabled () {
		BattleCharacter.onBattleCharacterSelected -= OnCharacterSelected;
	}

	void OnCharacterSelected (CharacterSheet sheet) {
		if ((affector.type == Affector.Type.SingleFriendly && sheet is PlayerSheet) ||
			(affector.type == Affector.Type.SingleEnemy && sheet is EnemySheet) ||
			(affector.type == Affector.Type.SingleAny)) {
			confirmButton.interactable = true;
			BattleCharacter.SelectCharacter(sheet);
			targets = new CharacterSheet[]{sheet};
		}
	}

	IEnumerator CheckForSelectionCoroutine () {
		while (enabled) {
			if (Input.GetMouseButton(0)) BattleCharacter.RaycastCharacters();
			yield return new WaitForEndOfFrame();
		}
	}

	public void ConfirmPressed () {
		if (targets != null && targets.Length > 0) {
			BattleCharacter.SetAllToDefault();
			BattleUIController.QueueAction(new ScheduledAction(source, targets, affector));
		}
	}
}
