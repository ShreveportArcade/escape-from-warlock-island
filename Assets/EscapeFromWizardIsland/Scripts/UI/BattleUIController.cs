﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Paraphernalia.Extensions;
using Paraphernalia.Utils;

public class BattleUIController : MonoBehaviour {

	public static BattleUIController instance;

	public Sprite circleSprite;

	public float slideInDuration = 2f;
	public Interpolate.EaseType easeType = Interpolate.EaseType.InQuad;

	public EnemyStatsUIController enemyStatsPrefab;
	public CharacterStatsUIController playerStatsPrefab;

	public ResultsScreenUIController resultsScreenUIController;

	public RectTransform backButtonRoot;
	public RectTransform actionMenuRoot;
	public RectTransform promptPanelRoot;
	public RectTransform enemyStatsRoot;
	public RectTransform playerStatsRoot;

	public Transform enemiesRoot;
	public Transform partyRoot;

	public Camera cam;

	private Dictionary<CharacterSheet, CharacterStatsUIController> sheetsToUIDict = new Dictionary<CharacterSheet, CharacterStatsUIController>();
	private List<ScheduledAction> scheduledActions = new List<ScheduledAction>();

	private PlayerSheet _currentCharacter;
	public PlayerSheet currentCharacter {
		get { return _currentCharacter; }
	}

	private List<RectTransform> menuStack = new List<RectTransform>();
	private RectTransform currentMenu {
		get {
			if (menuStack.Count > 0) {
				return menuStack[menuStack.Count - 1];
			}
			else {
				return null;
			}
		}
	}

	private List<EnemySheet> _enemies = new List<EnemySheet>();
	public static EnemySheet[] enemies {
		get {
			return instance._enemies.ToArray();
		}
	}
	public List<CharacterSheet> livingEnemies {
		get { 
			List<CharacterSheet> living = new List<CharacterSheet>(_enemies.ToArray());
			living.RemoveAll((e) => e.currentHealth <= 0);
			return living;
		}
	}

	public List<CharacterSheet> livingParty {
		get { 
			List<CharacterSheet> living = new List<CharacterSheet>(GameManager.instance.party);
			living.RemoveAll((p) => p.currentHealth <= 0);
			return living;
		}
	}
	
	public List<CharacterSheet> allLiving {
		get {
			List<CharacterSheet> living = new List<CharacterSheet>();
			living.AddRange(livingEnemies);
			living.AddRange(livingParty);
			return living;
		}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		}
		else {
			Destroy(gameObject);
		}
	}

	void OnEnable () {
		resultsScreenUIController.gameObject.SetActive(false);
		actionMenuRoot.gameObject.SetActive(false);
		promptPanelRoot.gameObject.SetActive(false);
		enemyStatsRoot.gameObject.SetActive(false);
		playerStatsRoot.gameObject.SetActive(false);
		backButtonRoot.gameObject.SetActive(false);
		SetupBattlefield();
		StartCoroutine("IntroAnimationCoroutine");
	}

	[ContextMenu("SetupBattlefield")]
	public void SetupBattlefield () {
		sheetsToUIDict = new Dictionary<CharacterSheet, CharacterStatsUIController>();
		_enemies = new List<EnemySheet>();

		enemyStatsRoot.DestroyChildren();
		enemiesRoot.DestroyChildren();
		int enemyCount = GameManager.instance.enemies.Length;
		Vector3 min = cam.ViewportToWorldPoint(new Vector3(0.55f, 0.45f, 0));
		Vector3 max = cam.ViewportToWorldPoint(new Vector3(0.875f, 0.45f, 0));
		for (int i = 0; i < enemyCount; i++) {
			EnemySheet sheet = Instantiate(GameManager.instance.enemies[i]) as EnemySheet;
			sheet.currentHealth = sheet.health;
			_enemies.Add(sheet);

			EnemyStatsUIController statsUI = enemyStatsPrefab.Instantiate() as EnemyStatsUIController;
			statsUI.Setup(sheet);
			statsUI.transform.SetParent(enemyStatsRoot);
			statsUI.transform.localScale = Vector3.one;

			sheetsToUIDict[sheet] = statsUI;

			GameObject enemy = SetupCharacter(sheet);
			enemy.transform.SetParent(enemiesRoot);
			enemy.transform.position = GetInitialPosition(min, max, enemyCount, i);
		}

		playerStatsRoot.DestroyChildren();
		partyRoot.DestroyChildren();
		int partySize = GameManager.instance.party.Length;
		min = cam.ViewportToWorldPoint(new Vector3(0.125f, 0.1f, 0));
		max = cam.ViewportToWorldPoint(new Vector3(0.45f, 0.1f, 0));
		for (int i = 0; i < partySize; i++) {
			PlayerSheet sheet = GameManager.instance.party[i];

			CharacterStatsUIController statsUI = playerStatsPrefab.Instantiate() as CharacterStatsUIController;
			statsUI.Setup(sheet);
			statsUI.transform.SetParent(playerStatsRoot);
			statsUI.transform.localScale = Vector3.one;

			sheetsToUIDict[sheet] = statsUI;

			GameObject character = SetupCharacter(sheet);
			character.transform.SetParent(partyRoot);
			character.transform.position = GetInitialPosition(min, max, partySize, i);
		}
	}

	GameObject SetupCharacter (CharacterSheet sheet) {
		GameObject character = null;
		if (sheet.battlePrefab != null) {
			character = sheet.battlePrefab.Instantiate() as GameObject;
		}
		else {
			character = new GameObject(sheet.characterName);
			SpriteRenderer s = character.AddComponent<SpriteRenderer>();
			s.sortingOrder = -1;
			s.sprite = sheet.image;
		}

		BattleCharacter b = character.GetOrAddComponent<BattleCharacter>();
		b.sheet = sheet;
		AddShadow(character.transform);
		return character;
	}

	Vector3 GetInitialPosition(Vector3 min, Vector3 max, int count, int index) {
		Vector3 pos = Vector3.zero;
		switch (count) {
			case 1: 
				pos = Vector3.Lerp(min, max, 0.5f);
				break;
			case 2:
				pos = Vector3.Lerp(min, max, 0.2f + index * 0.6f);
				break;
			default:
				pos = Vector3.Lerp(min, max, (float)index / ((float)count - 1f));
				break;
		}
		pos.z = 0;
		
		return pos;
	}

	IEnumerator IntroAnimationCoroutine () {
		Vector3 enemiesStart = cam.ViewportToWorldPoint(new Vector3(-2, 0.5f, 0));
		enemiesStart.z = 0;
		Vector3 partyStart = cam.ViewportToWorldPoint(new Vector3(2, 0.5f, 0));
		partyStart.z = 0;

		for (float t = 0; t < slideInDuration; t += Time.deltaTime) {
			float frac = Interpolate.Ease(easeType, t / slideInDuration);
			enemiesRoot.transform.position = Vector3.Lerp(enemiesStart, Vector3.zero, frac);
			partyRoot.transform.position = Vector3.Lerp(partyStart, Vector3.zero, frac);
			yield return new WaitForEndOfFrame();
		}

		enemiesRoot.transform.position = Vector3.zero;
		partyRoot.transform.position = Vector3.zero;

		BattleCharacter.SetupAllDefaults();

		ShowPrompt("Let's fight!");
		yield return new WaitForSeconds(2);
		HidePrompt();
		ShowRootMenu();
	}

	void AddShadow (Transform t) {
		GameObject shadow = new GameObject("shadow");
		shadow.transform.parent = t;
		shadow.transform.localPosition = Vector3.zero;
		shadow.transform.localScale = new Vector3(1.3f, 0.3f, 1);
		SpriteRenderer s = shadow.AddComponent<SpriteRenderer>();
		s.sprite = circleSprite;
		s.color = new Color(0,0,0,0.15f);
		s.sortingOrder = -2;
	}

	void AddMenuToStack (RectTransform menuTransform) {
		if (currentMenu != null) currentMenu.gameObject.SetActive(false);
		menuStack.Add(menuTransform);
		menuTransform.gameObject.SetActive(true);
		if (menuStack.Count > 1) backButtonRoot.gameObject.SetActive(true);
	}

	public void BackPressed () {
		currentMenu.gameObject.SetActive(false);
		menuStack.Remove(currentMenu);
		Debug.Log(currentMenu);
		currentMenu.gameObject.SetActive(true);
		if (menuStack.Count == 1) backButtonRoot.gameObject.SetActive(false);
	}

	public static void ShowPrompt (string text) {
		instance.promptPanelRoot.GetComponentInChildren<Text>().text = text;
		instance.promptPanelRoot.gameObject.SetActive(true);
		instance.enemyStatsRoot.gameObject.SetActive(false);
	}

	public static void HidePrompt () {
		instance.promptPanelRoot.gameObject.SetActive(false);
		instance.enemyStatsRoot.gameObject.SetActive(true);
	}

	public static void ShowRootMenu () {
		if (instance.currentMenu != null) instance.currentMenu.gameObject.SetActive(false);
		instance.menuStack = new List<RectTransform>();
		instance.menuStack.Add(instance.playerStatsRoot);
		instance.backButtonRoot.gameObject.SetActive(false);
		instance.playerStatsRoot.gameObject.SetActive(true);
	}

	void HideMenu () {
		currentMenu.gameObject.SetActive(false);
		backButtonRoot.gameObject.SetActive(false);
	}

	public static void ShowMenu (RectTransform transform) {
		instance.AddMenuToStack(transform);
	}

	public static void CharacterSelected(CharacterSheet sheet) {
		if (sheet is PlayerSheet) {
			instance._currentCharacter = sheet as PlayerSheet;
			instance.AddMenuToStack(instance.actionMenuRoot);
		}
	}

	public static void QueueAction(ScheduledAction scheduledAction) {
		instance.scheduledActions.Add(scheduledAction);
		SetCharacterActive(instance.currentCharacter, false);
		ShowRootMenu();
		
		if (instance.scheduledActions.Count == instance.livingParty.Count) {
			instance.StartAttackPhase();
		}
	}

	static void SetCharacterActive (CharacterSheet sheet, bool active) {
		instance.sheetsToUIDict[sheet].GetComponent<Button>().interactable = active;
	}

	void StartAttackPhase () {
		ShowRootMenu();

		foreach (CharacterSheet enemy in livingEnemies) {
			if (enemy.currentHealth > 0) {
				Affector affector = enemy.affectors[Random.Range(0, enemy.affectors.Length)];
				List<CharacterSheet> targets = new List<CharacterSheet>();
				switch (affector.type) {
					case Affector.Type.SingleFriendly:
						targets.Add(livingEnemies[Random.Range(0, livingEnemies.Count)]);
						break;

					case Affector.Type.AllFriendly:
						targets = livingEnemies;
						break;

					case Affector.Type.SingleEnemy:
						targets.Add(livingParty[Random.Range(0, livingParty.Count)]);
						break;

					case Affector.Type.AllEnemy:
						targets = livingParty;
						break;

					case Affector.Type.SingleAny:
						targets.Add(allLiving[Random.Range(0, allLiving.Count)]);
						break;

					case Affector.Type.All:
						targets = allLiving;
						break;

					default:
						break;
				}

				ScheduledAction scheduledAction = new ScheduledAction(enemy, targets.ToArray(), affector);
				scheduledActions.Add(scheduledAction);
			}
		}

		scheduledActions.Sort((a, b) => b.source.speed.CompareTo(a.source.speed));
		StartCoroutine("AttackPhaseCoroutine");
	}

	IEnumerator AttackPhaseCoroutine () {
		foreach (ScheduledAction action in scheduledActions) {
			if (action.source.currentHealth <= 0) continue;
			BattleCharacter.SelectCharacter(action.source);
			yield return new WaitForSeconds(0.7f);
			BattleAction battleAction = Instantiate(action.affector.battleActionPrefab);
			battleAction.source = action.source;
			battleAction.targets = action.targets;
			yield return battleAction.Run();
			bool[] hits = action.affector.ApplyChanges(action.source, action.targets);
			BattleCharacter.SetAllToDefault();

			for (int i = 0; i < action.targets.Length; i++) {
				CharacterSheet target = action.targets[i];
				if (i == action.targets.Length - 1) {
					yield return sheetsToUIDict[target].UpdateHealth();
				}
				else {
					sheetsToUIDict[target].UpdateHealth();
				}				
			}

			List<CharacterSheet> sheetsToCheck = new List<CharacterSheet>(action.targets);
			BattleCharacter[] chars = FindObjectsOfType(typeof(BattleCharacter)) as BattleCharacter[];
			int deaths = 0;
			foreach (BattleCharacter b in chars) {
				if (sheetsToCheck.Contains(b.sheet) && b.sheet.currentHealth <= 0) {
					deaths++;
					b.StartCoroutine("DeathCoroutine");
				}
			}

			if (deaths > 0) yield return new WaitForSeconds(2);

			yield return new WaitForSeconds(0.5f);
		}

		foreach (CharacterSheet sheet in GameManager.instance.party) {
			SetCharacterActive(sheet, true);
		}

		if (livingEnemies.Count == 0) {
			YouWin();
		}
		else if (livingParty.Count == 0) {
			YouLose();
		}
		else {
			ShowRootMenu();
		}

		scheduledActions.Clear();
	}

	void YouWin () {
		HideMenu();
		resultsScreenUIController.gameObject.SetActive(true);
		resultsScreenUIController.ShowWinScreen();
	}

	void YouLose () {
		HideMenu();
		resultsScreenUIController.gameObject.SetActive(true);
		resultsScreenUIController.ShowGameOverScreen();
	}

}
