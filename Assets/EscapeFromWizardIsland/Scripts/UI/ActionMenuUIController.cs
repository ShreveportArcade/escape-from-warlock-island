﻿using UnityEngine;
using System.Collections;

public class ActionMenuUIController : MonoBehaviour {

	public AffectorUIController affectorUIController;

	public void OnAttackPressed () {
		affectorUIController.affector = BattleUIController.instance.currentCharacter.weaponAffector;
		BattleUIController.ShowMenu(affectorUIController.GetComponent<RectTransform>());
	}

	public void OnMagicPressed () {
		Debug.Log("Magic");
	}

	public void OnItemPressed () {
		Debug.Log("Item");
	}

	public void OnRunPressed () {
		GameManager.ExitBattle();
	}
}
