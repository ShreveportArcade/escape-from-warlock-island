﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterStatsUIController : MonoBehaviour {

	public Slider healthBar;
	public Text nameText;
	public Text levelText;
	public float animationDuration = 0.7f;

	protected CharacterSheet sheet;

	public void Setup (CharacterSheet sheet) {
		this.sheet = sheet;
		nameText.text = sheet.characterName;
		levelText.text = "Lv." + sheet.level;
		healthBar.value = sheet.healthPct;
	}

	public Coroutine UpdateHealth () {
		return StartCoroutine("AnimateHealthBarCoroutine");
	}

	IEnumerator AnimateHealthBarCoroutine () {
		float startPct = healthBar.value;
		for (float t = 0; t < animationDuration; t += Time.deltaTime) {
			float frac = t / animationDuration;
			healthBar.value = Mathf.Lerp(startPct, sheet.healthPct, frac);
			yield return new WaitForEndOfFrame();
		}
	}

	public void CharacterSelected() {
		BattleUIController.CharacterSelected(sheet);
	}
}
