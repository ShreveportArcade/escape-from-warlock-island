﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuUIController : MonoBehaviour {

	public Button newGameButton;
	public Button continueButton;

	void Awake () {
		continueButton.interactable = false;
	}

	public void NewGamePressed () {
		GameManager.LoadLevel("Test");
	}

	public void ContinuePressed () {

	}
}
