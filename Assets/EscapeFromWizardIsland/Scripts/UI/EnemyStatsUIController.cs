﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyStatsUIController : CharacterStatsUIController {

	public Text typeText;
	
	public void Setup (EnemySheet sheet) {
		this.sheet = sheet;
		nameText.text = "Inmate #" + Random.Range(0,99999).ToString().PadLeft(5, '0');
		typeText.text = sheet.characterName;
		levelText.text = "Lv." + sheet.level;
		healthBar.value = sheet.healthPct;
	}
}
