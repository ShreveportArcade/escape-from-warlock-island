﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ResultsScreenUIController : MonoBehaviour {

	public Text resultsText;
	public Text promptText;

	public void ShowWinScreen () {
		StartCoroutine("ShowWinScreenCoroutine");
	}

	public void ShowGameOverScreen() {
		promptText.text = "";
		resultsText.text = "Game Over";
	}

	IEnumerator ShowWinScreenCoroutine () {
		int totalExp = 0;
		foreach (EnemySheet e in GameManager.instance.enemies) {
			totalExp += e.experience;
		}

		List<PlayerSheet> party = new List<PlayerSheet>();
		List<int> startExp = new List<int>();
		List<int> startLvl = new List<int>();
		foreach (PlayerSheet p in GameManager.instance.party) {
			if (p.currentHealth >= 0) {
				startExp.Add(p.experience);
				startLvl.Add(p.level);
				party.Add(p);
			}
		}
		int expEach = totalExp / party.Count;
		foreach (PlayerSheet p in party) {
			p.experience += expEach;
		}

		string txt = "";
		float duration = Mathf.Lerp(1, 10, Mathf.Clamp01((float)expEach / 100f));
		for (float t = 0; t < duration; t += Time.deltaTime) {
			txt = "";
			float frac = t / duration;
			for (int i = 0; i < party.Count; i++) {
				PlayerSheet p = party[i];
				txt += p.characterName;
				txt += " - Xp. " + Mathf.RoundToInt(Mathf.Lerp(startExp[i], p.experience, frac)).ToString();
				txt += " - Lv. " + Mathf.RoundToInt(Mathf.Lerp(startLvl[i], p.level, frac)).ToString();
				txt += "\n";				
			}
			resultsText.text = txt;
			yield return new WaitForEndOfFrame();
		}

		txt = "";
		for (int i = 0; i < party.Count; i++) {
			PlayerSheet p = party[i];
			txt += p.characterName;
			txt += " - Xp. " + p.experience.ToString();
			txt += " - Lv. " + p.level.ToString();
			txt += "\n";				
		}
		resultsText.text = txt;

		yield return new WaitForSeconds (2);

		GameManager.ExitBattle();
	}
}
