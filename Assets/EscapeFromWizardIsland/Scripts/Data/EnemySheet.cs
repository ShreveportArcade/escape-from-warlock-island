using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class EnemySheet : CharacterSheet {

	[Range(1, 100)] public int _level = 1;
	public override int level { 
		get { return _level; }
	}

	public AnimationCurve experienceCurve = AnimationCurve.Linear(1, 10, 100, 10000);
	public int experience { 
		get { return Mathf.CeilToInt(experienceCurve.Evaluate(level)); }
	}
}
