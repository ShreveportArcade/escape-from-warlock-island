using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class CharacterSheet : ScriptableObject {

	public string characterName = "Ace";
	[Multiline] public string description = "";

	[SerializeField] private int _currentHealth = 100;
	public int currentHealth {
		get { return Mathf.Clamp(_currentHealth, 0, health); }
		set { _currentHealth = Mathf.Clamp(value, 0, health); }
	}

	public Affector[] affectors;
	
	[ResourcePath(typeof(Sprite))] public string imagePath;
	private Sprite _image;
	public Sprite image {
		get {
			if (_image == null) {
				_image = Resources.Load(imagePath, typeof(Sprite)) as Sprite;
			}
			return _image;
		}
	}

	[ResourcePath(typeof(GameObject))] public string battlePrefabPath;
	private GameObject _battlePrefab;
	public GameObject battlePrefab {
		get {
			if (_battlePrefab == null) {
				_battlePrefab = Resources.Load(battlePrefabPath, typeof(GameObject)) as GameObject;
			}
			return _battlePrefab;
		}
	}

	public void UnloadBattleResources () {
		Resources.UnloadAsset(_image);
		_image = null;
		Resources.UnloadAsset(_battlePrefab);
		_battlePrefab = null;
	}
	
	public virtual int level { 
		get { return 100; }
	}

	public AnimationCurve healthCurve = AnimationCurve.Linear(1, 20, 100, 9999);
	public int health { 
		get { return Mathf.CeilToInt(healthCurve.Evaluate(level)); }
	}
	public float healthPct {
		get { return (float)currentHealth / (float)health; }
	}

	public AnimationCurve strengthCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int strength { 
		get { return Mathf.CeilToInt(strengthCurve.Evaluate(level)); }
	}

	public AnimationCurve defenseCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int defense { 
		get { return Mathf.CeilToInt(defenseCurve.Evaluate(level)); }
	}

	public AnimationCurve speedCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int speed { 
		get { return Mathf.CeilToInt(speedCurve.Evaluate(level)); }
	}

	public AnimationCurve specialCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int special { 
		get { return Mathf.CeilToInt(specialCurve.Evaluate(level)); }
	}

	public AnimationCurve accuracyCurve = AnimationCurve.Linear(1, 30, 100, 100);
	public int accuracy { 
		get { return Mathf.CeilToInt(accuracyCurve.Evaluate(level)); }
	}
}
