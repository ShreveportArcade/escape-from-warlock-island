﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class Item : ScriptableObject {

	public string itemName = "Potion";
	[Multiline] public string description = "Drink it.";
	public int inventory = 0;

	[ResourcePath(typeof(Affector))] public string affectorPath;
	private Affector _affector;
	public Affector affector {
		get {
			if (_affector == null) {
				_affector = Resources.Load(affectorPath, typeof(Affector)) as Affector;
			}
			return _affector;
		}
	}
}
