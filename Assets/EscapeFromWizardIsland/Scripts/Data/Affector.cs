﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class Affector : ScriptableObject {

	public string affectorName = "Punch";
	public string description = "";
	[Range(0, 100)] public float manaRequired = 0;
	[Range(-100, 100)] public float baseHealthChange = -10;
	[Range(0, 1)] public float accuracy = 0.9f;

	public enum Type {
		SingleFriendly,
		AllFriendly,
		SingleEnemy,
		AllEnemy,
		SingleAny,
		All
	}
	public Type type = Type.SingleEnemy;

	[ResourcePath(typeof(BattleAction))] public string battleActionPrefabPath;
	private BattleAction _battleActionPrefab;
	public BattleAction battleActionPrefab {
		get {
			if (_battleActionPrefab == null) {
				_battleActionPrefab = Resources.Load(battleActionPrefabPath, typeof(BattleAction)) as BattleAction;
			}
			return _battleActionPrefab;
		}
	}

	public bool[] ApplyChanges (CharacterSheet source, CharacterSheet[] targets) {
		bool[] hits = new bool[targets.Length];
		for (int i = 0; i < targets.Length; i++) {
			if (Random.value < accuracy) {
				hits[i] = true;
				float lvlRatio = (2f * (float)source.level + 10f) / 250f;
				CharacterSheet target = targets[i];
				int healthChange = 0;
				if (baseHealthChange < 0) {
					float hitRatio = (float)source.strength / (float)target.defense;
					healthChange = Mathf.FloorToInt(baseHealthChange * hitRatio * lvlRatio - 2);
				}
				else {
					healthChange = Mathf.CeilToInt(baseHealthChange * lvlRatio + 2);
				}
				target.currentHealth += healthChange;
			}
			else {
				hits[i] = false;
			}
		}
		return hits;
	}
}