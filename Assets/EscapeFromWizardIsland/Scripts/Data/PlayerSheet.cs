using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class PlayerSheet : CharacterSheet {

	[Range(0, 1000000)] public int experience = 0;

	public Affector weaponAffector;

	[ResourcePath(typeof(GameObject))] public string overworldPrefabPath;
	private GameObject _overworldPrefab;
	public GameObject overworldPrefab {
		get {
			if (_overworldPrefab == null) {
				_overworldPrefab = Resources.Load(overworldPrefabPath, typeof(GameObject)) as GameObject;
			}
			return _overworldPrefab;
		}
	}

	public void UnloadOverworldPrefab () {
		Resources.UnloadAsset(_overworldPrefab);
		_overworldPrefab = null;
	}
	
	public override int level { 
		get { return Mathf.Clamp(Mathf.FloorToInt(Mathf.Pow(experience, 1f/3f)), 1, 100); }
	}
}
