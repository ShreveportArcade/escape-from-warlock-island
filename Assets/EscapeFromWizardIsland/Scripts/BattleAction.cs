﻿using UnityEngine;
using System.Collections;

public class BattleAction : MonoBehaviour {

	public CharacterSheet source;
	public CharacterSheet[] targets;
	public float duration = 2;

	public Coroutine Run() {
		return StartCoroutine("ActionCoroutine");
	}
	
	protected virtual IEnumerator ActionCoroutine () {
		yield return new WaitForSeconds(duration);
	}
}
