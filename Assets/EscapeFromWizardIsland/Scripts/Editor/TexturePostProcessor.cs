﻿using UnityEngine;
using UnityEditor;

public class TexturePostProcessor : AssetPostprocessor {

	void OnPostprocessTexture(Texture2D texture) {
		TextureImporter importer = assetImporter as TextureImporter;
		importer.textureType = TextureImporterType.Sprite;
		importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		importer.filterMode = FilterMode.Point;
		importer.spritePixelsPerUnit = 16;

		Object asset = AssetDatabase.LoadAssetAtPath(importer.assetPath, typeof(Texture2D));
		if (asset != null) {
		    EditorUtility.SetDirty(asset);
		}
		else {
		    texture.filterMode = FilterMode.Point;          
		} 
	}
}