﻿using UnityEngine;
using System.Collections;

public class ScheduledAction {

	public CharacterSheet source;
	public CharacterSheet[] targets;
	public Affector affector;

	public ScheduledAction (CharacterSheet source, CharacterSheet[] targets, Affector affector) {
		this.source = source;
		this.targets = targets;
		this.affector = affector;
	}

}
