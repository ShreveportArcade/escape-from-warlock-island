Shader "Simple2D/Normal" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
			
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off Lighting Off ZWrite Off
		
		BindChannels {
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		Pass {
			SetTexture [_MainTex] {
				combine texture * primary
			}
		}
	}
	FallBack "Mobile/Particles/Alpha Blended"
}
